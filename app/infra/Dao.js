function Dao(connection) {
  this._connection = connection;
}

Dao.prototype.listaAutores = function(callback) {
  
  this._connection.infra.connection(function(err, connection) {

    if(err) {
      console.log(err);
      return;
    }

    connection.query('select * from autores', function(err, results) {
      connection.release();
      callback(err, results);
    });
  });
}

Dao.prototype.salva = function(tabela, dados, callback) {

  this._connection.infra.connection(function(err, connection) {

    if(err) {
      console.log(err);
      return;
    }

    connection.query(`insert into ${tabela} set ? `, dados, function(err, results) {
      connection.release();
      callback(err, results);
    });
  });
}

Dao.prototype.listaLivros = function(callback) {

  this._connection.infra.connection(function(err, connection) {

    if(err) {
      console.log(err);
      return;
    }

    connection.query('select livros.idLivro, livros.titulo, livros.preco, autores.nome as Autor from livros inner join autores on livros.idAutor = autores.idAutor',
    function(err, results) {
      connection.release();
      callback(err, results);
    });
  });
}

module.exports = function() {
  return Dao;
}