module.exports = function(app) {
  app.get('/livros', function(req, res) {
    
    let Dao = new app.infra.Dao(app);
    Dao.listaLivros(function(err, results) {

      if(err) {
        console.log(err);
        res.status(500).send(err);
        return;
      }

      res.status(200).json(results);
    });

  });

  app.post('/livros', function(req, res) {

    let livro = req.body;
    let Dao = new app.infra.Dao(app);
    Dao.salva('livros', livro, function(err, results) {

      if(err) {
        console.log(err);
        res.status(500).send(err);
        return;
      }
      
      res.status(201).json(results);
    });
  });
}