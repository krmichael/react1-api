module.exports = function(app) {
  app.get('/autores', function(req, res) {

    let Dao = new app.infra.Dao(app);
    
    Dao.listaAutores(function(err, results) {

      if(err) {
        console.log(err);
        res.status(500).send(err);
        return;
      }
      
      res.status(200).json(results);
    });

  });

  app.post('/autores', function(req, res) {

    let autor = req.body;
    let Dao = new app.infra.Dao(app);

    Dao.salva('autores', autor, function(err, results) {

      if(err) {
        console.log(err);
        res.status(500).send(err);
        return;
      }

      res.status(201).json(results);
    });
  });
}