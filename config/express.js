const express = require('express');
const app = express();
const consign = require('consign');
const bodyParser = require('body-parser');

module.exports = function() {

  app.use(bodyParser.json());

  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
  
  consign({cwd: 'app'})
    .include('routes')
    .then('infra')
    .into(app);

  return app;
}